# DevOps Glossary
 

## A

**Agent** — a part of the server-agent duo of programs, running in some instance or container to provide input for the centralized server app (like Zabbix monitoring agent)

**Agile software development** — a methodology of software delivery based on short iterative sprints of development, where every sprint should result in an operational product. This allows for easy adjustment of the project requirements should the need arise and empowers creativity and flexibility within the development teams.

**Artifact** — any process description in the software delivery pipeline, which can be referred to. The most widespread artifacts are use cases, class diagrams, UML models and design documents.

**Amazon AWS** — Amazon Web Services — the most popular cloud service provider (CSP) according to the State of DevOps report of 2017, offering a wide variety of cloud computing services for businesses of all sizes.

**Ansible** — an automation engine for various IT tasks, like cloud infrastructure provisioning and configuration. Ansible is an open source tool that interacts with multiple software modules via SSH connection, PowerShell scripts or various APIs.

**API** — Application Programming Interface, a set of clearly stated rules for communication between various software modules.

**Apache** — one of the most popular open source web servers (second only to NGINX), a cross-platform tool for launching websites and applications.

**ALB** — Application Load Balancing — an AWS service used for splitting the incoming traffic between multiple application instances in multiple Availability Zones to ensure your app runs smoothly.

**ASG** — Auto Scaling Group — an AWS service used for combining multiple EC2 instances in logical groups for the sake of infrastructure design and management simplicity; the group is comprised of identical instances that are added or removed to meet the workload demands.

**AWS CLI** — AWS Command Line Interface — an AWS tool for managing various AWS services and products from a command line terminal.

**Application release automation or ARA** — a general approach to deploying new code to production with as little human actions as possible, through automated CI/CD pipelines

**Amazon Aurora** — an AWS service, providing a cloud-based relational database, which became the most rapidly growing service in AWS history. This database is 5 times faster than MySQL and 3 times faster than PostgreSQL, not to mention it is a default database for many AWS products and services.

**Asterisk** — the open source platform for phone calls over the Internet. It is one of the most flexible solutions and can interact with multiple software tools via APIs, enabling multifunctional and efficient inter-business communications.

## B
**Bastion host** — a special server used to access private networks and withstand hacker attacks. Usually hosts a single app (like a proxy server) and SSH keys for accessing and managing the underlying cloud infrastructure.

**Branching** — branches are separate copies of the project code on GitHub or other code version control system, allowing many developers to work on the project at once.

**Bucket** — a logical unit in Amazon S3 (Simple Storage Service), used for storing multiple types of objects (mostly various data and the metadata that describes it).

**Backup** — a process of copying the important data to provide the reserve copy and enable restoration on demand; and the result of the backup process, an archive with files.

**Back-end** — the program engine that the user has no direct access to. This engine receives requests from the user interface and performs certain operations, including loading and saving the data to the database, etc.

**Build** — a specific version of program code, mostly referred to as the stage of new feature development. The most important builds are Canary builds, where the new code is tested for compliance with the existing app functionality in production.

**Bare-metal** — the case when the software is installed on the physical devices (hard disks), omitting the virtualization layer.

## C
**Canary release** — the staging server, which is an exact duplicate of the production environment. New software builds run there to ensure compliance with the existing features and code before rolling them out to the whole user base.

**Cloud computing** — a dominating IT paradigm of accessing over the Internet the networks of virtual servers for collecting, processing and storing data, running apps and managing other resources. An opposite to using dedicated servers or personal computers for that purpose.

**Continuous Delivery** — a set of software development and operations practices, workflows and tools aimed at ensuring all the routine operations of software delivery lifecycle are automated (builds, testing, staging, monitoring, alerting, logging, backups, restoration, load balancing, etc.) The only exception is the manual launch of the deployment of the ready code to the production environment. This is the core practice of DevOps, along with Continuous Integration and Infrastructure as Code.

**Continuous Deployment** — a particular case of Continuous Delivery, where the deployment of new code to production is also done automatically. This is not appropriate in some cases, though, and greatly depends on the particular requirements of your product and business model.

**CI/CD** — Continuous Integration/Continuous Delivery — the basis of the modern DevOps culture. CI ensures the new code is committed to the centralized code repository several times a day to pass automated unit tests and spin up the new software builds. If the tests are successful, CD ensures the new app version is automatically pushed to staging and production environments, without any service downtime. CI/CD workflow ensures all the bugs are found and fixed early and the product is available at all times.

**Cluster** — a set of interconnected instances (bare-metal servers, virtual machines, Kubernetes pods, etc.) that are treated as a single entity to enable load balancing, auto-scaling, and high availability.

**Commit** — the process of pushing the code to the Git repository and the resulting piece of code pushed.

**Cron job** — a scheduled process that will run a certain script on a server at a certain time.

**Container** — a software envelope separating the app and all resources required to run it from the infrastructure it runs on. Due to using Docker containers, any apps can run on any OS with Docker and any issues of a single container don’t affect the rest of the system.

**CloudWatch** — Amazon CloudWatch is the default Amazon service for monitoring the infrastructure and apps running on it. CloudWatch helps monitor and log the events, configure smart alerts and manage the system resources efficiently.

**Configuration drift** — the undesirable result of updating various servers independently, leading to different software configurations and states. Best removed through the practice of deployment of Immutable Infrastructure as Code.

**Configuration management** — the process of setting and maintaining the desired software ecosystem parameters with the help of automated configuration management tools like Kubernetes, Ansible, Puppet, Chef, Saltstack, etc.

## D
**DevOps** — the methodology of software delivery, as well as the set of practices, workflows, and tools required to ensure reliable automation of IT operations with a constant increase in quality.

**Dark launch** — a practice of releasing the code to the production environment without notifying the users of new available functionality. The code runs in production for a final round of testing, then the release of a new feature is announced, while the feature itself is already available.

**Docker** — an open source platform for building, delivering and running app containers. Docker is the basis of modern cloud computing, as it allows to leverage the cloud resources with utmost efficiency, providing a ubiquitous layer for building the cloud infrastructure.

**Dockerfile** — a textual description of the Docker container build process, containing all the commands required. Dockerfiles allow easy and efficient configuration and management of container creation and launch.

**Docker Swarm** — a container orchestration engine developed by Docker. It is a built-in Docker container clustering and scheduling tool capable of running thousands of containers at once. Unfortunately, it cannot boast the same functionality as Kubernetes and is literally out of use as of mid-2018.

**Deployment** — a stage of software delivery lifecycle, centered at packaging the new software code, delivering it to the customers and updating the running apps, preferably without interrupting the end user experience.

**Django framework** — a high-level Python framework oriented at clean design, rapid development and high performance of the apps. Has found wide adoption in web development and Big Data processing.

**Datadog** — an efficient cloud monitoring service, allowing to analyze the processes within any infrastructure, database or app at any scale, using a SaaS-based platform.

## E
**Environment** — all the server resources (OS, libraries, APIs, tools and frameworks, etc.) needed to run the software on various stages of its lifecycle (development, testing, staging, production).

**ElasticSearch** — a RESTful, distributed engine for data search and analytics, built on Apache Lucene. As a heart of Elastic stack, Elasticsearch allows to store and process the data from multiple cloud monitoring and logging tools.

**Envoy** — heavy-duty C++ proxy for processing the traffic between the microservices.

**EC2** — Amazon Elastic Compute Cloud — the central offer of Amazon Web Services, providing multiple types of virtual servers for running applications in the cloud.

**EKS** — Amazon Elastic Computer Service for Kubernetes — a managed Amazon service that allows anyone to deploy and run Kubernetes on AWS infrastructure without the need to look under the hood and configure the clusters themselves.

## F
**FluentD** — an open source data collection and processing tool written in Ruby. It allows input from a huge variety of tools like ElasticSearch and provides output to a wide selection of dashboards configured with multiple plugins.

**Fargate** — Amazon Fargate is an Amazon service for running Docker containers on managed infrastructure like EKS, without having to configure anything. It works under the serverless computing billing scheme — you specify what needs to be done and pay for the resources consumed, without any manual cluster configuration.

**Fail-fast** — the strategy of software design where the ideas are tested quickly to ensure rapid feedback. Once the feedback is applied, the experiment is repeated until the satisfactory result is achieved.

## G
**Git** — a distributed code version control system. Every developer using Git has access to a full copy of project code and history of changes to enable collaboration within the teams.

**GitHub** — the most popular web-based hosting for code, running all Git features and adding its own functionality. GitHub is the breathing heart of open-source and proprietary software development.

**GitLab** — an open source web-based Git portal tuned for DevOps performance, due to built-in support of CI/CD tools like Gitlab CI.

**Gitlab CI** — a CI/CD runner for Gitlab, which allows the developers to build their code automatically after each commit.

## H
**Helm** — an application manager running atop Kubernetes. This tool allows managing the microservices at scale through convenient Helm charts and ensures smooth operations of complex Kubernetes infrastructures.

## I
**Infrastructure** — the whole complex of hardware, software, and processes required to run apps, as well as to collect, manage and store data.

**IaC** — Infrastructure as Code — one of the basic principles of DevOps. It means that infrastructure configuration is done with machine-readable declarative files, not manually or using interactive tools. These files (like Kubernetes or Terraform manifests) can be stored in GitHub repositories, adjusted and versioned the same as code, thus providing efficient automation of infrastructure provisioning.

**IaaS** — Infrastructure-as-a-Service, the IT management model where the computing resources and the services needed to run them are provided as a service to enable the functioning of various platforms and apps.

**Image** — a Docker image is an immutable snapshot of a container, including the instructions on how to build a working Docker container for an app.

**InfluxDB** — an open source database for processing time series events. It is written in Go and used in infrastructure monitoring, high-availability data storage and real-time analytics. It works best with DevOps tools like Prometheus and Grafana.

**Instance** — in short, this is a virtual machine you run your app on. In broader terms, this is a single group of resources needed to run an app (like a Docker container, for example).

**I/O throughput** — the number of input/output operations per second, a characteristic of data transfer capacity of a network or a drive.

**Ingress controller** — a software module used to provide load balancing in Kubernetes pods.

## J
**Jenkins** — an open source Java server enabling software delivery automation out-of-the-box.

**Jenkins job** — a process in Jenkins, needed to build code, run unit tests, generate code quality metrics, deploy the new app versions to production, etc.

**Jump server** — a jump server, jump host or jump box is a system on a network used to access and manage devices in a separate security zone. A jump server is a hardened and monitored device that spans two dissimilar security zones and provides a controlled means of access between them

## K
**Kubernetes** — an open-source container management platform from Google. Kubernetes and Docker are the pillars of running modern workloads in the cloud.

**Kibana** — a part of the Elastic stack responsible for data visualization and navigating the ELK cluster.

## L
**Logstash** — a part of the Elastic stack responsible for server-side data collection, processing and transfer to the storage. Logstash is essential for building cloud monitoring solutions.

**Lead time** — the time needed to move the new code batch from commit to release.

## M
**Microservices** — an example of a service-oriented approach in software architecture (SOA), the practice of splitting the monolith application into a bunch of loosely coupled services responsible for a certain aspect of operations. These fine-grained services interact via lightweight protocols and APIs to provide product flexibility and scalability.

**MongoDB** — one of the best and most popular open source NoSQL databases specializing at storing various types of documents in forms of libraries, rather than in tables with columns and rows. This means the data stored can be easily adjusted and even the structure of the database itself is quite flexible, which is hugely beneficial for Big Data analytics projects, where there are multiple data types processes at various periods of time.

**MTTR** — Mean Time To Recovery — the average expected time for a failed system component to become operational again; the main parameter of failure recovery scenarios, system stress-testing and performance checks.

## N
**Node** — a physical or virtual machine within a Kubernetes cluster, used to host pods that run Docker containers.

**Node pool** — a Kubernetes node pool is a group of several cluster points uniting the machines with the same configuration, which can be essentially treated and managed as a single entity.

**Nexus3** — a release control platform from Sonatype, built for combining inputs from multiple open source modules to ensure fast, secure and efficient software delivery lifecycle.

**Nginx** — the most popular web server nowadays. The built-in capacities for load balancing, reverse caching and proxying make it a great choice for many use cases.

## O
**Orchestration** — a practice of automating the IT tasks (container management and infrastructure configuration in particular) in the context of SOA, virtualization, environment provisioning. In short, it is a process of executing predefined tasks using predefined scripts executed with interactive tools like Terraform (which was built specifically for configuration orchestration purposes).

**Open source** — the software delivery paradigm, where the copyright holders grant the users the access to the app source code and the rights to read, adjust and distribute it to anyone for any goal.

**OpenStack** — an open source platform for building on-prem cloud infrastructures.

**OpenShift** — enterprise-grade container management platform for Kubernetes running on on-prem cloud infrastructures, developed by Red Hat.

## P
**PaaS** — Platform-as-a-Service, the model of software delivery when the developers get all the required libraries, tools and services to develop the software, with all the underlying infrastructure being handled by the platform providing the service.

**Prometheus** — an open source cloud monitoring solution with a powerful query language, time series database, dimensional data model and smart alerting capabilities.

**Provisioning** — the process of delivering new environments for users, mostly the build and test environments for developers. As the resources are virtualized, the required operating systems and middleware are configured and maintained through configuration orchestration tools like Terraform and Kubernetes.

**Python** — an interpreted high-level programming language. Due to its efficiency and speed, Python is now widely adopted for tasks from website development to Big Data analytics.

**Pod** — a basic Kubernetes structure unit, a group of Docker containers deployed to a single host.
**Playbook** — Ansible playbooks are the instructions for infrastructure deployment, with detailed guides on executing the series of commands to perform specific tasks.

**ProxMox** — an open source Debian-based platform for deploying and managing virtual machines.

**Production environment** — the environment where the software product or service is used by the target audience.

## R
**RDS** — AWS Relational Database Service, a cloud database benefitting from a distributed nature of AWS services.

**Rolling update** — a process of smooth updates for an app without any downtime, performed instance by instance. It uses Kubernetes to ensure uninterrupted app availability and positive user experience.

**Rollback** — a manual or automated restoration of a previously saved state of program or database.

**Regression testing** — end-to-end testing of updated product version to ensure the latest build did not negatively impact the already available functionality.

**RabbitMQ** — a message broker, a software used to collect messages from your apps and store them until they are required for operations.

## S
**Source control** — the system for storing, managing and tracking the changes to the source code. The most popular are GitHub, GitLab, and BitBucket.

**S3** — Amazon Simple Storage Service — a cloud computing service for storing any data objects, required for stable operations of your applications.

**Snapshot** — Amazon EBS snapshot is a command for creating a static copy of your EC2 instance content for the purposes of backup and restoration.

**Staging environment** — the controlled copy of your production environment, resembling it to the fullest possible extent. This allows testing new software versions to find bugs before the release to production.

## T
**Terraform**  —  one of best tools for IaC to provision and manage any cloud, infrastructure, or service.

**Test automation** — the process of using a specific software to test the new software versions against unit tests and compare the actual test outcomes with the predicted results.

**Technical debt** — a concept of the undesired quantity of developer work required to correct the simple code used to gain fast results, instead of spending time designing and implementing the best solution.

## U
**Unit testing** — the basis of CI/CD, unit testing is the practice of testing the app code in small chunks against the automated test codebase before building the app, to minimize the time needed to discover and fix the bugs, reducing the time to market for a product as a result.

## V
**Virtual machine** — a basic unit of the cloud computing systems, an emulation of a physical server running under a supervisor.

**VPC peering** — AWS VPC is a service that logically isolates a certain amount of public AWS cloud to create virtual private clouds. AWS VPC peering allows to combine the resources of several such clouds should the need arise.

**Vault** — a Hashicorp product for securely storing the secrets like SSH keys, tokens, passwords, API keys and other important elements of Kubernetes infrastructure.

## Z
**Zabbix** — an open-source cloud infrastructure monitoring service for tracking the status of various network resources and services. Consists of a server and agents that enable smart alerting for distributed systems.



